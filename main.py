from functions import m_json
from functions import m_pck

#Um einfach die funktionen aufrufen zu kommen definiere ich Pfade als Variable
#Einmal für den heat_capacity, und einmal für den newton-Versuch.

#path für heat_capacity_versuch:
#setup_path = "datasheets/setup_heat_capacity.json"

#path für newton-Versuch:
path = "datasheets/setup_newton.json"

metadata = m_json.get_metadata_from_setup(setup_path)

#Aufrufen der add_temperature_sensor_serials-Funktion
m_json.add_temperature_sensor_serials("datasheets",metadata)

#Generieren der Messdaten und speichern der Daten.
data = m_pck.get_meas_data_calorimetry(metadata)

#Definieren der Eingangsvariablen für logging_calometry().
#Zunächst für den heat-capacity Versuch
#data_folder = "data/data_heat_capacity"
#json_folder = "datasheets"

#Jetzt für den Newton Versuch:
data_folder = "data/data_newton"
json_folder = "datasheets"

m_pck.logging_calorimetry(data,metadata,data_folder,json_folder)

#Ablegen der Daten in ein JSON File:
#Definieren der Variablen
folder_path = "datasheets"
#setup_path = siehe oben
#archiv_path = "data/data_heat_capacity"

#Für den Newton Versuch:
archiv_path = "data/data_newton"

m_json.archiv_json(folder_path, setup_path, archiv_path)